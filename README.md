## Push speed of TFTPRINTTT

Repeatedly sends 100 character strings, displaying the loop count
every four strings.  Intended to stress the circular buffer.
