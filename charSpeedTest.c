/*! \file  charSpeedTest.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 26, 2015, 9:45 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include "../include/graphicsHostLibrary.h"
#include "../include/colors.h"

/* Configuration fuses */
#pragma config FNOSC = FRCPLL       // Fast RC oscillator with PLL
#pragma config FWDTEN = OFF         // Turn off watchdog timer


/*! main - */

/*!
 *
 */
int main(void)
{
  int nLoopCount;

  /* Initialize stuff */
  serialInitialize(0);
  TFTinit(TFTLANDSCAPE);

  /* Opening display */
  TFTsetBackColorX(BLACK);
  TFTsetColorX(DARKSLATEGRAY);
  TFTsetFont(FONTDJS);
  TFTclear();
  putString(120, 100, 8, "Speed Test");
  delay(1000);

  nLoopCount = 0;
  TFTputsTT("\014");
  while(1)
    {
      TFTsetColorX(WHITE);
      TFTprintInt(nLoopCount,TFTPRINTINTZERO|TFTPRINTINTRIGHT,250,1);
      nLoopCount = nLoopCount+1;
      TFTsetColorX(DARKSLATEGRAY);
      TFTputsTT("When in the course of human events it becomes necessary for one people to dissolve the political bonds");
      TFTputsTT(" which have connected them to another, and to assume among the powers of the earth the separate and eq");
      TFTputsTT("ual station to which the laws of nature and of nature's god entitle them, grants that they should decl");
      TFTputsTT("are the causes which compel them to the separation. ");
    }
  return 0;
}
